FROM node:9.8.0

MAINTAINER Marti Markov <marti.markov@gmail.com>

RUN apt-get update

# Install awscli
RUN apt-get install -y python-dev python-pip

RUN pip install awscli